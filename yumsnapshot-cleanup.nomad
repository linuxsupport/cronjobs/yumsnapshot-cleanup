job "${PREFIX}_yumsnapshot-cleanup" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = false
  }

  task "${PREFIX}_yumsnapshot-cleanup" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/yumsnapshot-cleanup/yumsnapshot-cleanup:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_yumsnapshot-cleanup"
        }
      }
      volumes = [
        "$SNAPSHOTS:/yumsnapshot",
      ]
    }

    env {
      DATE = "$DATE"
      TAG = "${PREFIX}_yumsnapshot-cleanup"
    }

    resources {
      cpu = 100 # Mhz
      memory = 128 # MB
    }

  }
}
