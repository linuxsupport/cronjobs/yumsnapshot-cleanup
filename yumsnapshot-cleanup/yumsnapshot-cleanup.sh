#!/bin/bash
#
# delete old snapshots
#

DATE=`/bin/date +%Y%m%d --date="${DATE}"`
DPREF="/yumsnapshot"

echo "Starting yumsnapshot-cleanup for $DATE"

cd $DPREF
CANDIDATES=`find . -maxdepth 1 -type d -or -type l | grep -v "\.\$" | sed 's/\.\///' | sort | awk "{if(\\$1==\\$1+0 && \\$1<=$DATE)print \\$1}"`

for f in $CANDIDATES; do
  echo /bin/rm -rf $f
  /bin/rm -rf $f
done

# Help Ceph heal after (possibly) deleting primary copies of files
# by stat-ing the next snapshot date
NEXTDATE=`/bin/date +%Y%m%d --date="$DATE + 1 day"`
if [ -d $DPREF/$NEXTDATE ]; then
  echo time /bin/ls -flaR $DPREF/$NEXTDATE > /dev/null
  time /bin/ls -flaR $DPREF/$NEXTDATE > /dev/null
fi

echo "Finished!"
